package cxc.programacionymas.com.cxc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import cxc.programacionymas.com.cxc.io.MyApiAdapter
import cxc.programacionymas.com.cxc.io.responses.TaxisResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: TaxiAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
        getTaxisFromServer()
    }

    private fun setupRecyclerView() {
        mRecyclerView = findViewById(R.id.recycler_view)
        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(baseContext)
        mRecyclerView.layoutManager = layoutManager

        mAdapter = TaxiAdapter()
        mRecyclerView.adapter = mAdapter
    }

    private fun getTaxisFromServer() {
        val call: Call<TaxisResponse> = MyApiAdapter.getApiService().taxis
        call.enqueue(object : Callback<TaxisResponse> {
            override fun onFailure(call: Call<TaxisResponse>, t: Throwable) {
                Toast.makeText(baseContext, t.localizedMessage, Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<TaxisResponse>, response: Response<TaxisResponse>) {
                if (response.isSuccessful) {
                    val taxisResponse: TaxisResponse? = response.body()
                    if (taxisResponse != null) {
                        mAdapter.setDataSet(taxisResponse.taxis)
                    }
                }
            }

        })
    }

}
