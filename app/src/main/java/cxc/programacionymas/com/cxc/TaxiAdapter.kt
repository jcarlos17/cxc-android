package cxc.programacionymas.com.cxc

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import cxc.programacionymas.com.cxc.io.MyApiAdapter
import cxc.programacionymas.com.cxc.io.responses.SimpleResponse
import cxc.programacionymas.com.cxc.model.Taxi
import kotlinx.android.synthetic.main.item_taxi.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TaxiAdapter(
        private var mDataSet: ArrayList<Taxi>?
    ) : RecyclerView.Adapter<TaxiAdapter.ViewHolder>() {

    constructor() : this(null)

    fun setDataSet(taxis: ArrayList<Taxi>) {
        this.mDataSet = taxis
        this.notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindTaxi(taxi: Taxi) {
            itemView.tvTaxiId.text = itemView.context.getString(R.string.taxi_id, taxi.NUMERO_TAXI)
            itemView.tvPropietarioNr.text = taxi.NUMERO_PROPIETARIO
            itemView.tvPropietarioNombre.text = taxi.PROPIETARIO
            itemView.tvEstatus.text = taxi.ESTATUS
            itemView.btnToggle.text = if (taxi.ESTATUS == "A") "Desactivar" else "Activar"

            itemView.btnToggle.setOnClickListener {
                toggleStatusInServer(taxi)
            }
        }

        private fun toggleStatusInServer(taxi: Taxi) {
            val newStatus = if (taxi.ESTATUS == "A") "I" else "A"
            val call: Call<SimpleResponse> = MyApiAdapter.getApiService().toggleTaxiStatus(taxi.NUMERO_TAXI, newStatus)
            call.enqueue(object: Callback<SimpleResponse> {
                override fun onFailure(call: Call<SimpleResponse>, t: Throwable) {
                    Toast.makeText(itemView.context, t.localizedMessage, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<SimpleResponse>, response: Response<SimpleResponse>) {
                    if (response.isSuccessful) {
                        itemView.btnToggle.text = if (newStatus == "A") "Desactivar" else "Activar"
                        itemView.tvEstatus.text = newStatus
                        taxi.ESTATUS = newStatus
                        Toast.makeText(itemView.context, itemView.context.getString(R.string.successful_status_update), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(itemView.context, itemView.context.getString(R.string.unexpected_error), Toast.LENGTH_SHORT).show()
                    }

                }

            })
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxiAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_taxi, parent, false) as View


        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (mDataSet != null) {
            val taxi: Taxi = mDataSet!![position]
            holder.bindTaxi(taxi)
        }
    }

    override fun getItemCount(): Int {
        return mDataSet?.size ?: 0
    }
}