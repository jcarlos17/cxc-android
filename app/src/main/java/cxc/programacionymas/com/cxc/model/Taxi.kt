package cxc.programacionymas.com.cxc.model

class Taxi {
    lateinit var NUMERO_TAXI: String
    lateinit var NUMERO_PROPIETARIO: String
    lateinit var PROPIETARIO: String
    lateinit var PLACA: String
    lateinit var ESTATUS: String
}