package cxc.programacionymas.com.cxc.io;

import cxc.programacionymas.com.cxc.io.responses.SimpleResponse;
import cxc.programacionymas.com.cxc.io.responses.TaxisResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyApiService {

    @GET("taxis")
    Call<TaxisResponse> getTaxis();

    @GET("taxis/{id}/status")
    Call<SimpleResponse> toggleTaxiStatus(@Path("id") String taxiId, @Query("status") String status);

}